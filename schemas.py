asteroid_schema = {
    "type": "object",
    "required": [
        "type",
        "size"
    ],
    "properties": {
        "type": {
            "type": "string",
            "enum": ["asteroid"]
        },
        "size": {
            "type": "object",
            "required": [
                "x",
                "y"
            ],
            "properties": {
                "x": {
                    "type": "number",
                    "minimum": 1
                },
                "y": {
                    "type": "number",
                    "minimum": 1
                }
            }
        }
    }
}

robot_schema = {
    "type": "object",
    "required": [
        "type",
        "position",
        "bearing"
    ],
    "properties": {
        "type": {
            "type": "string",
            "enum": ["new-robot"]
        },
        "bearing": {
            "type": "string",
            "enum": ["north", "east", "south", "west"]
        },
        "position": {
            "type": "object",
            "required": [
                "x",
                "y"
            ],
            "properties": {
                "x": {
                    "type": "number",
                    "minimum": 0
                },
                "y": {
                    "type": "number",
                    "minimum": 0
                }
            }
        }
    }
}

movement_schema = {
    "type": "object",
    "required": [
        "type",
        "movement",
    ],
    "properties": {
        "type": {
            "type": "string",
            "enum": ["move"]
        },
        "movement": {
            "type": "string",
            "enum": ["turn-left", "turn-right", "move-forward"]
        }
    }
}

generic_schema = {
    "type": "object",
    "required": [
        "type"
    ],
    "properties": {
        "type": {
            "type": "string",
            "enum": ["asteroid", "new-robot", "move"]
        }
    }
}
