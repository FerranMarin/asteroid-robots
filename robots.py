import json
import os
import sys

from jsonschema import validate
from jsonschema.exceptions import ValidationError

from schemas import (
    asteroid_schema,
    generic_schema,
    movement_schema,
    robot_schema
)


class CommandReader:
    """ Command Reader, expects a file path that contains a
        json in each line with instructions for the Asteroid Robot """

    def __init__(self, file_path: str) -> None:
        """ Make sure the file exists and load commands into a list """
        self.ar = None
        if not os.path.isfile(file_path):
            raise Exception(f"File path {file_path} does not exist. "
                            f"Please provide a correct one")
        with open(file_path) as fp:
            self.cmd_list = [cmd.rstrip('\n') for cmd in fp]

    def process(self) -> None:
        """ Process the list of commands loaded at initializasion """
        for cmd in self.cmd_list:
            cmd = json.loads(cmd)
            self.validate_command(cmd)

        # We print final robot position once done with all commands
        self.ar.robot_position()

    def validate_command(self, data: dict) -> None:
        """ Validates against JSON schemas a given command """
        try:
            validate(instance=data, schema=generic_schema)
            if data["type"] == "asteroid":
                if self.ar:
                    raise Exception("Only 1 'asteroid' type of command "
                                    "per file is allowed.")
                validate(instance=data, schema=asteroid_schema)
                self.ar = AsteroidRobots(data["size"])
            elif data["type"] == "new-robot":
                validate(instance=data, schema=robot_schema)
                self.ar.new_robot(data)
            else:
                validate(instance=data, schema=movement_schema)
                self.ar.process_action(data["movement"])
        except ValidationError as ex:
            raise ex


class AsteroidRobots:
    """ Sets up an asteroid and controls the robots sent """

    _bearings = ["north", "east", "south", "west"]

    def __init__(self, data: dict) -> None:
        """ Setup asteroid size parameters """
        self.asteroid_x = data["x"]
        self.asteroid_y = data["y"]
        self.position = None
        self.bearing = None

    def new_robot(self, data: dict) -> None:
        """ Setup robot parameters and prints position
            if there was an existing robot """
        if self.position:
            self.robot_position()
        self.position = data["position"]
        self.bearing = data["bearing"]
        self.check_possible_locations()

    def robot_position(self):
        """ Prints out current robot position """
        print({
           "type": "robot",
           "position": self.position,
           "bearing": self.bearing
        })

    def process_action(self, move: str) -> None:
        """ Processes action command depending if
            it's a rotation or a move forward """
        if move == "move-forward":
            self.move_robot()
        else:
            self.rotate_robot(move)

    def rotate_robot(self, direction: str) -> None:
        """ Proceses turn command """
        bearing_index = self._bearings.index(self.bearing)
        if direction == "turn-right":
            try:
                self.bearing = self._bearings[bearing_index+1]
            except IndexError:
                # This is the west to north case
                self.bearing = self._bearings[0]
        else:
            self.bearing = self._bearings[bearing_index-1]

    def move_robot(self) -> None:
        """ Proceses move forward command """
        if self.bearing == "north":
            self.position["y"] += 1
        elif self.bearing == "south":
            self.position["y"] -= 1
        elif self.bearing == "east":
            self.position["x"] += 1
        elif self.bearing == "west":
            self.position["x"] -= 1
        self.check_possible_locations()

    def check_possible_locations(self) -> None:
        "Checks if robot is out of bounds"
        if (
            self.position["y"] > self.asteroid_y
            or self.position["y"] < 0
            or self.position["x"] > self.asteroid_x
            or self.position["x"] < 0
        ):
            raise Exception("Robot would be out of bounds.")


if __name__ == "__main__":
    try:
        json_file = sys.argv[1]
    except IndexError:
        raise Exception("Please supply a file with instructions")

    CommandReader(json_file).process()
