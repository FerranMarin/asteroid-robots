# Asteroid Robots Show Me The Robots!

## Problem Explanation
The European Space Agency (ESA) is planning to send some robots to an asteroid. The surface of the asteroid is represented as a rectangular two dimensional grid.

A robot's position on an asteroid is represented by a pair of co-ordinates and its current bearing. An example position might be (1, 3, South) which means that the robot is one mile East and three miles North of the asteroid's origin and is facing South.

The co-ordinate of the position one place North of the asteroid's origin (0, 0) is (0, 1).

In order to control a robot, the ESA sends a series JSON messages, each one on a new line.

The allowed message types are:

- A message stating the size of the asteroid - this will always be the first message
- A message stating the position of a new robot
- A message telling the current robot to move

A message stating the size of an asteroid looks like:

```{"type": "asteroid", "size": {"x": 5, "y": 5}}```

A new robot message looks like:

```{"type": "new-robot", "position": {"x": 0, "y": 1}, "bearing": "north"}```

A movement message looks like:

```{"type": "move", "movement": "turn-left"}```

The other allowed values for `"movement"` are `"turn-right"` and `"move-forward"`.


## The Program
Your program should consume a series of JSON commands provided in a text file, one line per command.

Your program should output a series of JSON messages, each on a new line, describing the final positions of the robots after all of the input commands have been executed.

An output message should look like this, with one  message per robot:

```{"type": "robot": , "position": {"x": 7, "y": 3}, "bearing": "south"}```

Please make your program as easy to run as possible and if necessary include clear instructions.

Your program should receive messages from a text file passed as an argument and should output messages on stdout, for example:

```
$ python robots.py instructions.txt
{"type": "robot", "position": {"x": 1, "y": 3}, "bearing": "north"}
{"type": "robot", "position": {"x": 5, "y": 1}, "bearing": "east"}

```


## Worked example
### Input

```
{"type": "asteroid", "size": {"x": 5, "y": 5}}
{"type": "new-robot", "position": {"x": 1, "y": 2}, "bearing": "north"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-left"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "move-forward"}
{"type": "new-robot", "position": {"x": 3, "y": 3}, "bearing": "east"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "move-forward"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "turn-right"}
{"type": "move", "movement": "move-forward"}
```

### Output

```
{"type": "robot", "position": {"x": 1, "y": 3}, "bearing": "north"}
{"type": "robot", "position": {"x": 5, "y": 1}, "bearing": "east"}
```

# Solution

## Setup -- Poetry

The solution for this has been setup using [poetry](https://python-poetry.org/). Easiest way to install poetry is with the following command:

```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

After that, poetry takes care of dependencies and its own virtual environment. So, after installing poetry you need to install the dependencies:

```
poetry install
```

Finally, to run any command, simply:

```
poetry run <comand to run using the virtual env for the project>
# Example to run the program
poetry run python robots.py input.txt
# Example to run tests
poetry run pytest
# Example to run tests in verbose mode
poetry run pytest -v
```

## Setup -- Non Poetry

There is a requirements.txt file included in case one wants to run this code withou the use of Poetry. 
I would recommend doing the following in this case:

```
python3 -m venv venv
source activate venv
pip install -r requirements.txt
# To run the program
python robots.py input.txt
# To run tests
python -m pytest
# Tests in verbose mode
python -m pytest -v
```


## Asumptions

In this solution I've made the following assumptions:
- Given an asteroid size, there are no possible negative numbers and trying to go beyond the max coordinates results in an error. For example: Given an asteroid size: {"x": 2, "y": 2} if for any reason a robot would be in x or y 3, it would be out of bounds and the program will exit with an error message.
- Each time we move forward we move 1 mile in the bearing direction.
- In each instructions file, there can only be 1 "asteroid" type command and it will be the 1st one.
