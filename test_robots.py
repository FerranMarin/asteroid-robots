import os

import pytest
from mock import patch
from robots import AsteroidRobots, CommandReader


def test_invalid_file_input():
    """ Verify that a file input which is not a valid file gets an
        Exception """

    with pytest.raises(Exception) as ex:
        CommandReader("file.txt")

    assert (str(ex.value) ==
            'File path file.txt does not exist. '
            'Please provide a correct one')


def test_required_file_arg():
    """ Verify that the file arg is required """

    with pytest.raises(TypeError) as err:
        CommandReader()

    assert (str(err.value) ==
            "__init__() missing 1 required positional argument: "
            "'file_path'")


@patch("robots.AsteroidRobots")
def test_command_reader_process_instanciates_asteroid(mocked_class,
                                                      tmpdir):
    """ Verify process instanciates asteroid correctly """
    fh = tmpdir.join("text.txt")
    fh.write('{"type": "asteroid", "size": {"x": 5, "y": 5}}')
    filename = os.path.join(fh.dirname, fh.basename)
    CommandReader(filename).process()
    mocked_class.assert_called_with({"x": 5, "y": 5})


@patch("robots.AsteroidRobots")
def test_command_reader_process_two_asteroids(mocked_class, tmpdir):
    """ Verify process raises exception if two asteroid instructions
        are in the file provided """
    fh = tmpdir.join("text.txt")
    fh.write('{"type": "asteroid", "size": {"x": 5, "y": 5}} \n'
             '{"type": "asteroid", "size": {"x": 2, "y": 2}}')
    filename = os.path.join(fh.dirname, fh.basename)
    with pytest.raises(Exception) as ex:
        CommandReader(filename).process()
        mocked_class.assert_called_with({"x": 5, "y": 5})
        mocked_class.assert_called_once()

    assert (str(ex.value) ==
            "Only 1 'asteroid' type of command per file is allowed.")


@patch("robots.AsteroidRobots.new_robot")
def test_command_reader_process_new_robot(mocked_new_robot, tmpdir):
    """ Verify process calls for a new robot correctly """
    fh = tmpdir.join("text.txt")
    fh.write('{"type": "asteroid", "size": {"x": 5, "y": 5}} \n'
             '{"type": "new-robot", "position": {"x": 1, "y": 2}, '
             '"bearing": "north"}')
    filename = os.path.join(fh.dirname, fh.basename)
    CommandReader(filename).process()
    mocked_new_robot.assert_called_with(
        {
          "type": "new-robot",
          "position": {"x": 1, "y": 2},
          "bearing": "north"
        }
    )


@patch("robots.AsteroidRobots.process_action")
def test_command_reader_process_action(mocked_process_action, tmpdir):
    """ Verify process calls for a process_action correctly """
    fh = tmpdir.join("text.txt")
    fh.write('{"type": "asteroid", "size": {"x": 5, "y": 5}} \n'
             '{"type": "move", "movement": "turn-left"}')
    filename = os.path.join(fh.dirname, fh.basename)
    CommandReader(filename).process()
    mocked_process_action.assert_called_with("turn-left")


@patch("robots.AsteroidRobots.robot_position")
def test_command_reader_robot_position_end(mocked_robot_position,
                                           tmpdir):
    """ Verify process calls for a robot position at the end """
    fh = tmpdir.join("text.txt")
    fh.write('{"type": "asteroid", "size": {"x": 5, "y": 5}} \n'
             '{"type": "new-robot", "position": {"x": 1, "y": 2}, '
             '"bearing": "north"} \n'
             '{"type": "move", "movement": "turn-left"}')
    filename = os.path.join(fh.dirname, fh.basename)
    CommandReader(filename).process()
    mocked_robot_position.assert_called()


def test_asteroid_robots_init():
    ar = AsteroidRobots({"x": 1, "y": 2})
    assert ar.asteroid_x == 1
    assert ar.asteroid_y == 2
    assert not ar.position
    assert not ar.bearing


def test_first_new_robot():
    ar = AsteroidRobots({"x": 5, "y": 5})
    ar.new_robot({"type": "new-robot",
                  "position": {"x": 3, "y": 3},
                  "bearing": "east"})
    assert ar.position == {"x": 3, "y": 3}
    assert ar.bearing == "east"


@patch("robots.AsteroidRobots.robot_position")
def test_next_new_robot(mocked_robot_position):
    ar = AsteroidRobots({"x": 5, "y": 5})
    ar.new_robot({"type": "new-robot",
                  "position": {"x": 3, "y": 3},
                  "bearing": "east"})
    assert ar.position == {"x": 3, "y": 3}
    assert ar.bearing == "east"
    ar.new_robot({"type": "new-robot",
                  "position": {"x": 1, "y": 2},
                  "bearing": "north"})
    mocked_robot_position.assert_called()
    assert ar.position == {"x": 1, "y": 2}
    assert ar.bearing == "north"


@patch('builtins.print')
def test_robot_position(mocked_print):
    ar = AsteroidRobots({"x": 5, "y": 5})
    ar.new_robot({"type": "new-robot",
                  "position": {"x": 3, "y": 3},
                  "bearing": "east"})
    assert ar.position == {"x": 3, "y": 3}
    assert ar.bearing == "east"
    ar.robot_position()
    mocked_print.assert_called_with({"type": "robot",
                                     "position": {"x": 3, "y": 3},
                                     "bearing": "east"})


@pytest.mark.parametrize("move_type, function",
                         [
                           ("turn-left", "rotate_robot"),
                           ("turn-right", "rotate_robot"),
                           ("move-forward", "move_robot"),
                         ])
def test_process_action(move_type, function):
    with patch("robots.AsteroidRobots."+function) as mocked_fn:
        ar = AsteroidRobots({"x": 5, "y": 5})
        ar.process_action(move_type)
        mocked_fn.assert_called_once()


@pytest.mark.parametrize("init_b, final_b",
                         [
                           ("north", "east"),
                           ("east", "south"),
                           ("south", "west"),
                           ("west", "north")
                         ])
def test_process_rotate_right(init_b, final_b):
    ar = AsteroidRobots({"x": 5, "y": 5})
    ar.new_robot({"type": "new-robot",
                  "position": {"x": 3, "y": 3},
                  "bearing": init_b})
    ar.rotate_robot("turn-right")
    assert ar.bearing == final_b


@pytest.mark.parametrize("init_b, final_b",
                         [
                           ("north", "west"),
                           ("east", "north"),
                           ("south", "east"),
                           ("west", "south")
                         ])
def test_process_rotate_left(init_b, final_b):
    ar = AsteroidRobots({"x": 5, "y": 5})
    ar.new_robot({"type": "new-robot",
                  "position": {"x": 3, "y": 3},
                  "bearing": init_b})
    ar.rotate_robot("turn-left")
    assert ar.bearing == final_b


@pytest.mark.parametrize("bearing, final_pos",
                         [
                           ("north", {"x": 3, "y": 4}),
                           ("east", {"x": 4, "y": 3}),
                           ("south", {"x": 3, "y": 2}),
                           ("west", {"x": 2, "y": 3})
                         ])
def test_move(bearing, final_pos):
    ar = AsteroidRobots({"x": 5, "y": 5})
    ar.new_robot({"type": "new-robot",
                  "position": {"x": 3, "y": 3},
                  "bearing": bearing})
    ar.move_robot()
    assert ar.position == final_pos


@pytest.mark.parametrize("location",
                         [
                           ({"x": 6, "y": 4}),
                           ({"x": 5, "y": 6}),
                           ({"x": -1, "y": 5}),
                           ({"x": 2, "y": -1})
                         ])
def test_check_possible_locations(location):
    ar = AsteroidRobots({"x": 5, "y": 5})
    with pytest.raises(Exception) as ex:
        ar.position = location
        ar.check_possible_locations()

    assert str(ex.value) == "Robot would be out of bounds."


def test_worked_example(capsys):
    """ Verify Worked Example from README returns expected output """

    CommandReader("input.txt").process()
    captured = capsys.readouterr()

    assert not captured.err
    assert (captured.out ==
            "{'type': 'robot', 'position': {'x': 1, 'y': 3}, "
            "'bearing': 'north'}\n{'type': 'robot', "
            "'position': {'x': 5, 'y': 1}, 'bearing': 'east'}\n")
