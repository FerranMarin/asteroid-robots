import pytest
from jsonschema import validate
from jsonschema.exceptions import ValidationError

from schemas import (
    asteroid_schema,
    generic_schema,
    movement_schema,
    robot_schema
)


@pytest.mark.parametrize("data",
                         [
                           ({"type": "wrong"}),
                           ({"kind": "move"}),
                         ])
def test_validate_general_schema_raises(data):
    with pytest.raises(ValidationError):
        validate(instance=data, schema=generic_schema)


@pytest.mark.parametrize("data",
                         [
                           ({"type": "move"}),
                           ({"type": "asteroid"}),
                           ({"type": "new-robot"})
                         ])
def test_validate_general_schema_valid(data, capsys):
    validate(instance=data, schema=generic_schema)
    captured = capsys.readouterr()

    assert not captured.out
    assert not captured.err


@pytest.mark.parametrize("data",
                         [
                           ({"type": "asteroid",
                             "size": {"x": 0, "y": 0}}),
                           ({"type": "asteroid",
                             "size": {"x": -2, "y": 1}}),
                           ({"type": "asteroid",
                             "size": {"x": 1, "y": -5}}),
                           ({"type": "asteroid",
                             "size": {"x": -1, "y": -1}}),
                           ({"size": {"x": 1, "y": 1}}),
                           ({"type": "asteroid"}),
                         ])
def test_validate_asteroid_schema_raises(data):
    with pytest.raises(ValidationError):
        validate(instance=data, schema=asteroid_schema)


@pytest.mark.parametrize("data",
                         [
                           ({"type": "asteroid",
                             "size": {"x": 5, "y": 5}}),
                           ({"type": "asteroid",
                             "size": {"x": 2, "y": 100}}),
                           ({"type": "asteroid",
                             "size": {"x": 1, "y": 5}}),
                         ])
def test_validate_asteroid_schema_valid(data, capsys):
    validate(instance=data, schema=asteroid_schema)
    captured = capsys.readouterr()

    assert not captured.out
    assert not captured.err


@pytest.mark.parametrize("data",
                         [
                            ({"type": "new-robot",
                              "position": {"x": -1, "y": 2},
                              "bearing": "north"}),
                            ({"type": "new-robot",
                              "position": {"x": 1, "y": 2},
                              "bearing": "wrong"}),
                            ({"type": "new-robot",
                              "position": {"x": 1, "y": -2},
                              "bearing": "north"}),
                            ({"type": "new-robot",
                              "position": {"x": -4, "y": -1},
                              "bearing": "north"}),
                            ({"type": "new-robot",
                              "position": {"x": 1, "y": 2}}),
                            ({"type": "new-robot",
                              "bearing": "north"}),
                            ({"position": {"x": 2, "y": 2},
                              "bearing": "north"}),
                         ])
def test_validate_robot_schema_raises(data):
    with pytest.raises(ValidationError):
        validate(instance=data, schema=robot_schema)


@pytest.mark.parametrize("data",
                         [
                            ({"type": "new-robot",
                              "position": {"x": 1, "y": 2},
                              "bearing": "north"}),
                            ({"type": "new-robot",
                              "position": {"x": 5, "y": 5},
                              "bearing": "south"}),
                            ({"type": "new-robot",
                              "position": {"x": 2, "y": 2},
                              "bearing": "east"}),
                            ({"type": "new-robot",
                              "position": {"x": 3, "y": 4},
                              "bearing": "west"}),
                            ({"type": "new-robot",
                              "position": {"x": 0, "y": 0},
                              "bearing": "north"}),
                            ({"type": "new-robot",
                              "position": {"x": 1000, "y": 222},
                              "bearing": "north"})
                         ])
def test_validate_robot_schema_valid(data, capsys):
    validate(instance=data, schema=robot_schema)
    captured = capsys.readouterr()

    assert not captured.out
    assert not captured.err


@pytest.mark.parametrize("data",
                         [
                           ({"type": "moves", "movement": "turn-left"}),
                           ({"type": "move", "movement": "wrong"}),
                           ({"movement": "turn-left"}),
                           ({"type": "move"})
                         ])
def test_validate_movement_schema_raises(data):
    with pytest.raises(ValidationError):
        validate(instance=data, schema=movement_schema)


@pytest.mark.parametrize("data",
                         [
                           ({"type": "move", "movement": "turn-left"}),
                           ({"type": "move",
                             "movement": "move-forward"}),
                           ({"type": "move", "movement": "turn-right"})
                         ])
def test_validate_movement_schema_valid(data, capsys):
    validate(instance=data, schema=movement_schema)
    captured = capsys.readouterr()

    assert not captured.out
    assert not captured.err
